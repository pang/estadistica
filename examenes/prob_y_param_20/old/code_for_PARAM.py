# -*- encoding: utf-8 -*-
import random
import math
import scipy.stats as st

class ProblemaNormal2D():
    def __init__(self):
        self.N = 50
        self.mediaE = 15
        self.mediaG = 20
        self.stdE = 3
        self.stdG = random.randint(2,6)
        self.corr = (1+2*random.randint(-4,4))/10
        self.varE = self.stdE**2
        self.varG = self.stdG**2
        self.cov = round(self.stdE*self.stdG*self.corr,2)
#        print(self.corr, self.cov)
        self.E2 = self.mediaE**2 + self.varE
        self.G2 = self.mediaG**2 + self.varG
        self.EG = self.cov + self.mediaE* self.mediaG
        self.data_type = random.randint(0,1)
        self.gasto_total_mayor_que = self.mediaE + self.mediaG + self.stdE
        self.mediaEmasG = self.mediaE + self.mediaG
        self.varEmasG = self.varE +self.varG +2*self.cov
        self.stdEmasG = math.sqrt( self.varEmasG)
        self.stdEmasGround = round(math.sqrt( self.varEmasG),3)
        N = st.norm(loc=self.mediaEmasG, scale= self.stdEmasG)
        self.probapartado2 = round(1 - N.cdf( self.gasto_total_mayor_que),3)
        self.gasto_actividades_descuento = self.mediaE + 1
        self.gasto_actividades_mayor_que = self.mediaE + self.stdE + 1
        A = st.norm(loc=self.mediaE, scale= self.stdE)
        self.probapartado3 = round(
            (1 - A.cdf( self.gasto_actividades_mayor_que))/(1 - A.cdf( self.gasto_actividades_descuento)),
            3)

    def data(self):
        #TODO: distintos tipos de información (correlacion, EG, etc)
#        if self.data_type:
        return r'''N=%s,
        \frac{1}{N}\sum_i^N A_i = %s,
        \frac{1}{N}\sum_i^N P_i = %s,
        \frac{1}{N}\sum_i^N A_i^2 = %s,
        \frac{1}{N}\sum_i^N P_i^2 = %s,
        \frac{1}{N}\sum_i^N A_i\cdot P_i = %s'''%(
        self.N,
        self.mediaE, self.mediaG,
        self.E2, self.G2, self.EG)

class ProblemaOrca():
    def __init__(self):
        self.a = random.randint(3,10)
        self.b = random.randint(3,10)
        self.mu = self.a/self.b
        self.d = random.randint(1,4)
        self.nobservaciones = 12 + self.d
        if random.randint(0,1):
            self.priorname = 'uniforme'
            self.priora = 1
        else:
            self.priorname = 'de Jeffreys'
            self.priora = 1/2
        self.priorb = 0
        self.posta = (self.priora+self.nobservaciones)
        self.postb = (self.priorb+16)
        O = st.gamma(a=self.priora+self.nobservaciones, scale=1/(self.priorb+16))
        self.intervalo90 = (round(O.ppf(0.05),3), round(O.ppf(0.95),3))

class ProblemaInundacion():
    def __init__(self):
        self.tmedio = 10*random.randint(18,22)
        self.tasa = 1/self.tmedio
        self.deadline1 = 100
        T = st.expon(scale=self.tmedio)
        if random.randint(0,1):
            self.pregunta1 = 'Calcula la probabilidad de que la próxima inundación ocurra antes de 100 años.'
            self.codigo1 = 'T.cdf(%s)'%self.deadline1
            self.prob1 = T.cdf( self.deadline1)
        else:
            self.pregunta1 = 'Calcula la probabilidad de que la próxima inundación no ocurra antes de 100 años.'
            self.codigo1 = '1 - T.cdf(%s)'%self.deadline1
            self.prob1 = 1 - T.cdf( self.deadline1)

        self.media5 = 5*self.tmedio
        self.std5 = math.sqrt(5*self.tmedio**2)
        T5 = st.norm(loc = self.media5,scale=self.std5)
        self.deadline2 = 800
        if random.randint(0,1):
            self.pregunta2 = 'Aproxima la probabilidad de que sea necesario emprender la reforma antes de 800 años'
            self.codigo2 = 'T5.cdf(%s)'%self.deadline2
            self.prob2 = T5.cdf( self.deadline2)
        else:
            self.pregunta2 = 'Aproxima la probabilidad de que no sea necesario emprender la reforma antes de 800 años'
            self.codigo2 = '1 - T5.cdf(%s)'%self.deadline2
            self.prob2 = 1 - T5.cdf( self.deadline2)
