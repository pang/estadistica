import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as st

def plot_discrete_dist(D, span=None):
    '''Dibuja la función de masa de una distribución discreta
    como diagrama de barras

    ARGS::
    
    - D: una distribución de probabilidad de scipy.stats
    '''
    span = span or (
        int(np.floor(D.ppf(0.001))),
        int(np.ceil (D.ppf(0.999)))
    )
    xs = range(span[0],span[1]+1)
    plt.bar(xs,[D.pmf(k) for k in xs],
                fill=False)
    docstring = D.dist.__doc__
    first_line_index = docstring.find('\n')
    plt.title("Mass function of %s"%docstring[:first_line_index])   

def plot_cdf(D, span=None):
    '''Dibuja la función de distribución de una distribución discreta

    ARGS::
    
    - D: una distribución de probabilidad de scipy.stats
    '''
    span = span or (
        min(0,int(np.floor(D.ppf(0.001)))),
        int(np.ceil (D.ppf(0.999)))
    )
    # al menos 300 puntos son necesarios porque es discontinua
    xs = np.linspace(span[0],span[1]+1,300)
    plt.plot(xs,D.cdf(xs))
    docstring = D.dist.__doc__
    first_line_index = docstring.find('\n')
    plt.title("Distribution function of %s"%docstring[:first_line_index])   

def dibuja_distribucion_empirica(muestra, dist=None):
    '''Dibuja la función de distribución empírica de una muestra.
    
    - muestra: array de numpy
    - dist (opcional): distribución de scipy.stats contra la que comparar
                       la muestra

    Cómo dibujamos la función de distribución empírica:

    - Ordenamos los elementos de la muestra: ``sorted(muestra)``
    - Añadimos un valor a la izquierda de todos los valores de la muestra, 
      y otro a la derecha de todos los valores de la muestra 
      ``np.concatenate([[minx], sorted(muestra), [maxx]])``
    - Definimos ``pasoy = 1/len(muestra)``, que es el incremento de la 
      función de distribución cada vez que x supera un elemento de ``muestra``.
    - ``np.arange(0,1,paso)`` contiene los valores [0, paso, 2*paso..., 1-paso]
    - Añadimos el valor 1 para el último valor de la muestra, y para el punto 
      maxx que pusimos a mano: ``np.concatenate( [np.arange(0,1,paso), [1,1]])`` 
    - En vez de ``plt.plot``, que dibuja la función lineal a trozos que pasa por
      varios puntos usamos ``plt.step``, que dibuja una función escalonada (step 
      function).
    '''
    L = max(muestra) - min(muestra)
    minx = min(muestra)-.1*L
    maxx = max(muestra)+.1*L
    if dist:
        mean, std = dist.mean(), dist.std()
        minx = min(minx, mean-3*std)
        maxx = max(maxx+.1*L, mean+3*std)
    xs = np.concatenate([[minx], sorted(muestra), [maxx]])
    pasoy = 1/len(muestra)
    ys = np.concatenate( [np.arange(0,1,pasoy), [1,1]])
    plt.step(xs, ys, where='post', label='cdf empírica')
    if dist:
        xs = np.arange(minx,maxx,std*0.01)
        ys = dist.cdf(xs)
        plt.plot(xs,ys, label = 'cdf')
    plt.legend()
    plt.show()


## continuas

def dibuja_normal(mean, std, x1=None, x2=None):
    N = st.norm(loc=mean, scale=std)
    xmin, xmax = mean-3*std, mean+3*std
    if x1 is not None:
        x1b = max(x1, mean-5*std)
        xmin = min(xmin, x1b)
    if x2 is not None:
        x2b = min(x2, mean+5*std)
        xmax = max(xmax, x2b)
    xs = np.arange(xmin, xmax, 0.01)
    ys = N.pdf(xs)
    plt.figure(figsize=(10,6))
    plt.plot(xs,ys, label='(%.1f,%.1f)'%(mean,std), lw=2, color='blue')
    plt.fill_between(xs, 0, ys, alpha = 0.2, color = 'blue')
    if (x1 is not None) and (x2 is not None) and x1<x2:
        xs2 = np.arange(max(x1,xmin), min(x2, xmax), 0.01)
        ys2 = N.pdf(xs2)
        area = N.cdf(x2)-N.cdf(x1)
        plt.fill_between(xs2, 0, ys2, alpha = 0.2, color = 'green')
        plt.text((x1b+x2b)/2, (N.pdf(x1b) +N.pdf(x2b))/4,
                 '$P(x\in [%.2f,%.2f])=%.2f$'%(x1,x2,area),
                 horizontalalignment='center',
                 verticalalignment='center')
    else:
        plt.text(mean, N.pdf(mean)/2,'$P(x\in \mathbb{R})=1$',
                 horizontalalignment='center',
                 verticalalignment='center')
    plt.autoscale(tight=True)

def plot_normals(PARAMS):
    xs = np.arange(min(mean-3*std for mean,std in PARAMS),
                   max(mean+3*std for mean,std in PARAMS),0.01)
    plt.figure(figsize=(16,12))
    for mean, std in PARAMS:
    #    ys = [st.norm.pdf(x, mean, std) for x in xs]
        ys = st.norm.pdf(xs, mean, std)
        lines = plt.plot(xs,ys, label='(%.1f,%.1f)'%(mean,std), lw=2)
        plt.fill_between(xs, 0, ys, alpha = 0.2, color = lines[0].get_color())
        plt.autoscale(tight=True)
    plt.ylim(0)
    plt.legend(loc = 'upper left', title="($\mu$, $\sigma$) parameters")

def plot_exponentials(PARAMS):
    xs = np.arange(0, 5,0.01)
    plt.figure(figsize=(16,12))
    for r in PARAMS:
    #    ys = [st.norm.pdf(x, mean, std) for x in xs]
        ys = st.expon.pdf(xs, scale=1/r)
        lines = plt.plot(xs,ys, label='%.1f'%r, lw=2)
        plt.fill_between(xs, 0, ys, alpha = 0.2, color = lines[0].get_color())
        plt.autoscale(tight=True)
    plt.ylim(0)
    plt.legend(loc = 'upper right', title="$\lambda$ parameters")    

def compare_sample_dist(sample, dist):
    plt.hist(sample, density=True)
    xs = np.linspace(dist.ppf(0.005), dist.ppf(0.995))
    ys = dist.pdf(xs)
    plt.plot(xs, ys)

# discretas
        
def plot_samples_discrete(samples, rango, pmf):
    '''Recibe como argumentos:
    - una lista de tuplas (tamaño, muestra)
    - una distribución de probabilidad de scipy.stats
    Dibuja un histograma de cada muestra junto con la función de masa,
    dispuestas horizontalmente
    '''
    N = len(samples)
    samples_sizes = [k for k,_ in samples]
#    rango = max(max(sample) for _,sample in samples) + 1
    fig, axes = plt.subplots(nrows=1, ncols=N,figsize=(4*N, 6))
    axs = axes.flatten()
    for j,ax,(k,s) in zip(range(N),axs, samples):
        ax.hist(
            s,
            bins=[k+0.5 for k in range(-1,rango+1)], 
            density=1,
            histtype='bar',
            color=(0,j/N,1-j/N)
        )
        ax.bar(range(rango),[pmf(k) for k in range(rango)],
                fill=False)
        ax.set_title(u'Tamaño de la muestra: %.1e'%k)

def compara(P1, label1, P2, label2):
    rango= max(int(P1.ppf(0.9999)), int(P2.ppf(0.9999)))
    plt.bar(range(rango),[P1.pmf(k) for k in range(rango)],
            color='blue', alpha = 0.5, label=label1)
    plt.bar(range(rango),[P2.pmf(k) for k in range(rango)],
            color='green', alpha = 0.5, label=label2)
    plt.legend()
    
## continuas_TCL

def simulacion_suma(dist1, dist2, K=1000, mostrar_suma=False):
    fig, axes = plt.subplots(nrows=1, ncols=3,figsize=(12, 6))
    axs = axes.flatten()
    for dist,ax in zip((dist1,dist2), axs):
        mean, std = dist.mean(), dist.std()
        xs = np.arange(mean-3*std,mean+3*std,std*0.01)
        ys = dist.pdf(xs)
        ax.plot(xs,ys)

    muestra = dist1.rvs(K) + dist2.rvs(K)
    axs[-1].hist(muestra, density=1, histtype='bar',
             bins=int(K/30))
    if mostrar_suma:
        mean = dist1.mean() + dist2.mean()
        std  = np.sqrt(dist1.var()  + dist2.var())
        xs = np.arange(mean-3*std,mean+3*std,std*0.01)
        dist = st.norm(loc=mean, scale=std)
        ys = dist.pdf(xs)
        axs[-1].plot(xs,ys, color='r', 
                     label='N(%.2f,%.2f)'%(mean, std))
        axs[-1].legend()
    axs[-1].set_title('Extraccion de la suma')

def promedio_extracciones(dist, ns, n0, K=1000):
    N = len(ns)
    fig, axes = plt.subplots(nrows=1, ncols=N,figsize=(4*N, 6))
    axs = axes.flatten()
    for j,ax,n in zip(range(N),axs, ns):
        h = 1/np.sqrt(n)
        nn = int(n0/h)
        bins = [h*(k+0.5) for k in range(-1,nn+1)]
        color = (0,j/N,1-j/N)
        sums = [sum(dist.rvs() for _ in range(n))/n
                for _ in range(K)]
        ax.hist(sums, density=1, histtype='bar',
                 color=color,
                 bins=bins,
                 label='Promedio de %d VAIs'%n)

        try:
            mean, std = dist.mean(), dist.std()
            xs = np.arange(mean-3*std,mean+3*std,std*0.01)
            ys = dist.pdf(xs)
        #    ys = st.norm.pdf(xs, mean, std)
            ax.plot(xs,ys)
        except AttributeError:
            if n0:
                ax.bar(range(n0+1),[dist.pmf(k) for k in range(n0+1)],fill=False)

        ax.legend()

def promedio_extracciones_TCL(dist, ns, K=1000):
    N = len(ns)
    mean, std = dist.mean(), dist.std()
    xs = np.arange(mean-4*std,mean+4*std,std*0.01)

    fig, axes = plt.subplots(nrows=1, ncols=N,figsize=(16, 12))
    for n,ax in zip(ns, axes):
        sums = [sum(dist.rvs() for _ in range(n))/n
            for _ in range(K)]
        ax.hist(sums, density=1, histtype='bar',
                label=str(n))

        ys = st.norm.pdf(xs, mean, std/np.sqrt(n))
        ax.plot(xs,ys, label='TCL')
        try:
            ys = dist.pdf(xs)
        #    ys = st.norm.pdf(xs, mean, std)
            ax.plot(xs,ys, label='original')
        except:
            pass
        ax.legend()
        
def plot_samples_continuous(samples, dist, nbins=10):
    N = len(samples)
    samples_sizes = [k for k,_ in samples]
    mean, std = dist.mean(), dist.std()
    xs = np.arange(mean-3*std,mean+3*std,std*0.01)
    ys = dist.pdf(xs)

    nrows = int(np.sqrt(N))
    ncols = int(np.ceil(N/nrows))
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols,figsize=(16, 12))
    axs = axes.flatten()
    for ax,(k,s) in zip(axs, samples):
        ax.hist(s, density=1, bins=nbins)
        ax.plot(xs,ys)
        ax.set_title('N=%d'%k)

percentiles_canonicos = [
    1-2*st.norm(loc=0,scale=1).cdf(-k) for k in (1,2,3)
]
def plot_multinormal(MN, sample=None, sample_size=1000, npoints=100):
    #Muestra aleatoria de la normal multivariable
    if sample is None:
        sample = MN.rvs(sample_size)
    means, Sigma = MN.mean, MN.cov
    # Create grid coordinates for plotting
    std1, std2 = np.sqrt([Sigma[0,0], Sigma[1,1]])
    X1 = np.linspace(means[0] - 4*std1, means[0] + 4*std1, npoints)
    X2 = np.linspace(means[1] - 4*std2, means[1] + 4*std2, npoints)
    xx, yy = np.meshgrid(X1,X2, indexing='xy')
    Z = np.zeros((X1.size,X2.size))

    for i,x1i in enumerate(X1):
        for j, x2j in enumerate(X2):
            Z[j,i] = MN.pdf([x1i,x2j])

    fig = plt.figure(figsize=(15,6))
    fig.suptitle('Normal multivariable', fontsize=20)
    #Queremos dibujar exactamente los contornos que capturan 
    # los percentiles habituales (~68%,95%,99.7%):
    #https://en.wikipedia.org/wiki/Multivariate_normal_distribution#Interval
    factor = 1/(2*np.pi*np.sqrt(np.linalg.det(Sigma)))
    E = st.expon(scale=2).ppf
    levels = [
        factor*np.exp(-0.5*E(p))
        for p in reversed(percentiles_canonicos)
    ]

    plt.contour(xx, yy, Z, cmap=plt.cm.Set2_r, levels=levels)
    plt.contourf(xx, yy, Z, cmap=plt.cm.Set2_r, alpha=0.4, levels=levels)
    plt.scatter(sample[:,0], sample[:,1], color='k', zorder=10, s=10, alpha=0.8)


