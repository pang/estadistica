{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import scipy.stats as st"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sumas de variables aleatorias\n",
    "\n",
    "Comenzamos con una pregunta natural:\n",
    "\n",
    "Si conocemos la distribución que siguen las VAs $X$ e $Y$: ¿cuál es la distribución de $X+Y$?\n",
    "\n",
    "Al hablar de distribuciones bidimensionales hemos visto que para modelizar el vector $(X,Y)$ necesitamos una descripción conjunta, y no es suficiente con estudiar la distribución de $X$ y la de $Y$ por separado.\n",
    "La distribución de $X+Y$ también depende de la relación entre ambas variables."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Variables aleatorias independientes\n",
    "\n",
    "Dos variables aleatorias son independientes si y sólo si:\n",
    "$$\n",
    "P(X\\in A | Y \\in B) = P(X\\in A)\n",
    "$$\n",
    "o también\n",
    "$$\n",
    "P(X\\in A \\cap Y \\in B) = P(X\\in A)P(Y\\in B)\n",
    "$$\n",
    "para cualesquiera subconjuntos $A$ del soporte de $X$ y $B$ del soporte de $Y$.\n",
    "Para distribuciones continuas, es equivalente a decir que la función de densidad conjunta es el producto de las funciones de densidad:\n",
    "$$\n",
    "f(x,y) = f_X(x)f_Y(y)\n",
    "$$\n",
    "La relación entre ambas formulaciones la podemos ver integrando:\n",
    "$$\n",
    "\\begin{split}\n",
    "P(X\\in A \\cap Y \\in B) &=  \n",
    "\\int_{A\\times B}f(x,y)dxdy\\\\\n",
    "P(X\\in A)P(Y\\in B)&=\\int_{A}f_X(x)dx\\int_{B}f_Y(y)dy\n",
    "= \\int_{A\\times B}f_X(x)f_Y(y)dxdy\n",
    "\\end{split}\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Si dos variables $X$ e $Y$ son independientes**, entonces\n",
    "$$\n",
    "E[XY] =  \\int xyf(x,y)dxdy\n",
    "=  \\int xyf_X(x)f_Y(y)dxdy\n",
    "=  \\left(\\int xf_X(x)dx\\right) \\left(\\int yf_Y(y)dy\\right)\n",
    "=E[X]E[Y]\n",
    "$$\n",
    "y se sigue que\n",
    "$$\n",
    "Cov[X,Y] = E[(X-\\mu_X)(Y-\\mu_Y)] = E[XY] - \\mu_X\\mu_Y = 0\n",
    "$$\n",
    "y por tanto también, su *correlación es 0*.\n",
    "Además:\n",
    "$$\n",
    "Var[X + Y] = E[(X+Y-\\mu_X-\\mu_Y)^2] = ... = Var[X] + Var[Y]\n",
    "$$\n",
    "Pero cuidado:\n",
    "\n",
    "> *Dos variables aleatorias cuya correlación es cero no son necesariamente independientes.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "**Si X e Y son VAs independientes, y siguen distribuciones normales, su suma sigue una distribución normal.**\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*¿Qué media y desviación tiene la normal obtenida?*\n",
    "\n",
    "Recordamos que **siempre se verifica**:\n",
    "\n",
    "$$\n",
    "E[X + Y] = E[X] + E[Y]\n",
    "$$\n",
    "Por lo visto anteriormente, **si las variables son independientes**:\n",
    "$$\n",
    "Var[X + Y] = Var[X] + Var[Y]\n",
    "$$\n",
    "y ésto significa que si $X\\sim N(\\mu_X,\\sigma_X)$, $Y\\sim N(\\mu_Y,\\sigma_Y)$, entonces:\n",
    "$$\n",
    "X + Y \\sim N\\left(\\mu=\\mu_X + \\mu_Y, \\sigma=\\sqrt{\\sigma_X^2 + \\sigma_Y^2}\\right)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Suma de \"n\" normales independientes\n",
    "\n",
    "Sumamos ``n`` normales *indepedientes* $X_i$ con la misma media $\\mu$ y la misma desviación típica $\\sigma$.\n",
    "\n",
    "- **VAIs**: Acrónimo para *Variables Aleatorias Independientes*.\n",
    "- **VAIIDs**: Acrónimo para *Variables Aleatorias Independientes e Idénticamente Distribuidas*. Podemos pensar que vaiids son *\"distintos individuos de una misma población\"*.Por ejemplo, todos los atunes de cierto mar, antes de pescarlos, son equivalentes: si sacamos uno del mar y lo pesamos, el peso seguirá una distribución normal con la misma media y la misma desviación típica, *pero cada atún tiene un peso distinto*. Lo mismo aplica a bombillas de la misma fábrica, alturas de personas adultas, etc...\n",
    "\n",
    "Si $X_i$ es el peso del atún i-ésimo, el peso total de una muestra de ``n`` atunes es:\n",
    "$$\n",
    "S=\\Sigma_{i=1}^n X_i\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*¿Qué media y desviación tiene la suma de ``n`` variables normales $X_i$?*\n",
    "\n",
    "Por lo visto anteriormente:\n",
    "$$\n",
    "S \\sim N(\\mu_S = \\Sigma_{i=1}^n \\mu, \\sigma_S^2 = \\Sigma_{i=1}^n \\sigma_X^2)\n",
    "= N(n\\mu, \\sqrt{n}\\sigma_X)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Promedio de \"n\" normales indepedientes\n",
    "Si además dividimos por ``n`` para obtener el promedio:\n",
    "$$\n",
    "\\frac{1}{n}S = \\frac{1}{n}\\Sigma_{i=1}^n X_i \n",
    "\\sim N(\\mu, \\frac{1}{\\sqrt{n}}\\sigma_X)\n",
    "$$\n",
    "En particular, **si n es muy grande**, el promedio es aleatorio, pero cada vez oscila menos en torno a la media."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ley de los grandes números y teorema central del límite\n",
    "\n",
    "Lo anterior no solo es cierto para variables normales, sino que _también es aproximadamente cierto para casi todas las distribuciones_.\n",
    "\n",
    "> **Ley de los grandes números**: *Cuando hacemos promedios de extracciones aleatorias de VAIs $X_i$ que tienen todas media $\\mu$, el valor promedio converge a la media de las variables $X_i$:*\n",
    "\n",
    "$$\n",
    "E[X_i]=\\mu \\Rightarrow \\frac{1}{n}\\Sigma_{i=1}^n X_i \\rightarrow \\mu\n",
    "$$\n",
    "\n",
    "No vamos a definir qué queremos decir exactamente al hablar de \"convergencia de variables aleatorias\". Hay más de una forma de medir esta convergencia, y por eso hay más de una forma de la ley de los grandes números (en particular, la *ley fuerte de los grandes números* y la *ley débil de los grandes números*).\n",
    "\n",
    "https://en.wikipedia.org/wiki/Law_of_large_numbers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ejemplo. \n",
    "\n",
    "Repetimos $n$ veces un experimento Bernoulli con probabilidad de éxito $p$,  y llamamos $X$ a la variable aleatoria que cuenta el número de éxitos. Entonces por la Ley de los grandes números \n",
    "$$\n",
    "P\\left(\\left|\\frac Xn-p\\right|<\\varepsilon\\right)_{n\\to\\infty}\\hskip-15pt\\longrightarrow 1\n",
    "$$\n",
    "\n",
    "Con lo que si queremos estimar el parámetro $p$ bastará con repetir el experimento un número muy grande de veces."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Teorema central del límite\n",
    "\n",
    "En vez de tomar el promedio podemos afinar un poco más:\n",
    "Si $X_i$  son variables aleatorias independientes con media y varianza finitas:\n",
    "\n",
    " - $E[X_i] = \\mu$\n",
    " - $Var[X_i] = \\sigma^2$\n",
    "\n",
    "Entonces:\n",
    "\n",
    "$$\n",
    "\\frac{{\\frac {1}{n}}\\sum_{i=1}^{n}X_{i}-\\mu}{\\frac{\\sigma}{\\sqrt{n}}}\\longrightarrow\n",
    "\\mathcal{N}(0,1)\n",
    "$$\n",
    "\n",
    "o, dicho de otra forma:\n",
    "\n",
    "$$\n",
    "{\\displaystyle {\\bar {X}}={\\frac {1}{n}}\\sum _{i=1}^{n}X_{i}} \\approx\n",
    "\\mathcal{N}(\\mu, \\sigma^2/n)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "El teorema central del límite es incluso cierto si las variables aleatorias independientes no tienen la misma media y la misma desviación (y se cumplen algunas condiciones técnicas que no vamos a comentar):\n",
    "\n",
    "> **Una suma $S=\\Sigma_{i=1}^n X_i$ de variables aleatorias independientes $X_i$ se puede aproximar por una normal con la misma media y la misma varianza que $S$**\n",
    "\n",
    "$$\n",
    "E[S] = \\Sigma_{i=1}^n E[X_i],\\quad Var[S] = \\Sigma_{i=1}^n Var[X_i]\n",
    "$$ \n",
    "\n",
    "*Es por eso que tantos fenómenos naturales siguen una distribución gaussiana* (y es por eso que a la distribución gaussiana se la llama distribución **normal**).\n",
    "\n",
    "Por ejemplo, el tiempo que tarda un autobús en hacer el recorrido es la suma de los tiempos que tarda en cada parada, más los tiempos en cada semáforo, etc, donde cada uno de esos tiempos sigue una distribución uniforme, o quizá exponencial, pero la suma de todos los tiempos es aproximadamente normal."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Para más información sobre la ley de los grandes números y el teorema central del límite, os recomendamos:\n",
    "\n",
    " - [Chapter 4 of Bayesian Methods for Hackers](https://nbviewer.jupyter.org/github/CamDavidsonPilon/Probabilistic-Programming-and-Bayesian-Methods-for-Hackers/blob/master/Chapter4_TheGreatestTheoremNeverTold/Ch4_LawOfLargeNumbers_PyMC3.ipynb)\n",
    " - [The most dangerous equation](http://nsmn1.uh.edu/dgraur/niv/TheMostDangerousEquation.pdf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jupyter": {
     "outputs_hidden": true
    }
   },
   "source": [
    "En el caso de un experimento de Bernoulli, el teorema central del límite se reduce al teorema de [Moivre-Laplace](https://es.wikipedia.org/wiki/Teorema_de_De_Moivre-Laplace), que nos permite calcular números combinatorios difíciles gracias a la fórmula\n",
    "$$\n",
    "\\binom{n}{k}p^k(1-p)^{n-k}\\sim\\frac{1}{\\sqrt{2\\pi np(1-p)}}e^{-\\frac{(x-np)^2}{2np(1-p)}}.\n",
    "$$\n",
    "Concretamente si $X$ es una variable aleatorial binomial de parámetros $n$, $p$, con $np(1-p)$ grande, entonces \n",
    "$P(X=k)=P(k-1/2<Y\\le k+1/2)$, y $P(k_1\\le X\\le k_2)=P(k_1-1/2<Y\\le k_2+1/2)$, donde $Y$ es una variable aleatoria $N(np,\\sqrt{np(1-p)})$, y la probabilidad se puede calcular por medio de las tablas de la normal."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ejemplo \n",
    "\n",
    "Un display tiene $10x15$ pixeles. Cada pixel se ilumina independientemente del resto, con  probabilidad $0.4$. ¿Cuál es la probabilidad de que haya mas de $80$ pixeles iluminados?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "El número de pixeles iluminados $X$ sigue una binomial $B(150,0.4)$, que se puede aproximar por una normal $N\\sim N(\\mu=150\\cdot0.4, \\sigma=\\sqrt{150\\cdot0.4\\cdot0.6})$ con lo que la probabilidad de que haya más de $80$ es\n",
    "$$\n",
    "P(X>80)=P(N>80.5)=1-P(N\\le 80.5)\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.000316964234919781"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s = (150*0.4*0.6)**(1/2)\n",
    "m = 150*0.4\n",
    "N = st.norm(m,s)\n",
    "1 - N.cdf(80.5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ejercicio \n",
    "\n",
    "En una transmisión digital la probabilidad de recibir un bit erróneamente es $p =10^{-5}$, y se supone que los errores de transmisión son independientes. Suponiendo una\n",
    "transmisión de $16$ millones de bits, calcular la probabilidad de que se produzcan más\n",
    "de $150$ errores."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
