{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import random\n",
    "import numpy as np\n",
    "import scipy as sc\n",
    "import scipy.stats as st\n",
    "\n",
    "import pandas as pd\n",
    "\n",
    "import seaborn as sns\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Inferencia estadística\n",
    "\n",
    "La Teoría de la Probabilidad nos permite modelizar procesos complejos que involucran fenómenos aleatorios que se combinan de formas arbitrarias.\n",
    "\n",
    "En la práctica, nuestro conocimiento experto o experiencia previa nos puede ayudar a elegir un enfoque, una técnica estadística, una familia paramétrica... adecuadas al fenómeno a modelizar.\n",
    "\n",
    "Sin embargo, cualquier modelo lo bastante flexible como para merecer un espacio en el repertorio de la estadística necesitará un ajuste al fenómeno particular que se pretende modelizar. Esta fase se suele llamar últimamente _\"entrenamiento del modelo\"_, usando un término que viene del campo del aprendizaje automático, aunque se usaba en la estadística desde mucho antes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Inferencia paramétrica\n",
    "\n",
    "Las familias de distribuciones de probabilidad que hemos estudiado (Poisson, Bernoulli, Normal...) permiten modelizar de forma decente una cantidad sorprente de fenómenos del mundo real, pero necesitan que fijemos los valores de los parámetros. La inferencia paramétrica es una cualquiera de las técnicas que permiten dar valores a los parámetros desconocidos del modelo elegido.\n",
    "\n",
    "Por ejemplo: en una estación de servicio entran vehículos, de una forma compatible con un proceso de Poisson. Es muy posible que el tiempo necesario para atender a cada vehículo siga aproximadamente una distribución normal. Nos podemos preguntar cuál es el tiempo medio que espera un vehículo, la probabilidad de que espere más de 15 minutos, etc... pero necesitamos conocer la tasa del proceso de Poisson de entrada de vehículos, y la media y desviación típica de la distribución normal del tiempo de servicio."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Los objetivos de la _inferencia paramétrica_ son: \n",
    " - encontrar el modelo probabilístico que describe mejor el fenómeno\n",
    " - determinar los parámetros del modelo\n",
    " - criticar si el modelo propuesto es correcto.\n",
    "\n",
    "Un **estadístico** es un valor numérico que se calcula a partir de los valores de los datos, e intenta dar información del parámetro que se quiere estimar. El valor del estadístico, por tanto, es aleatorio, pero esperamos que nos sirva para aproximar a uno de los parámetros desconocidos."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Máxima verosimilitud\n",
    "\n",
    "La técnica de **máxima verosimilitud (maximum likelihood)** es una forma popular de *encontrar los parámetros de distribuciones paramétricas a partir de observaciones del fenómeno aleatorio que modelizan*.\n",
    "\n",
    "Dado que usamos **datos** obtenidos midiendo el mundo real, para sacar **conclusiones** que luego queremos usar en modelos matemáticos, es una técnica de **inferencia estadística**. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "El fenómeno aleatorio $X$ sigue una distribución paramétrica $X\\sim \\mathcal{D}(\\lambda)$, donde $\\mathcal{D}(\\lambda)$ es una familia de distribuciones, pero _no conocemos el parámetro $\\lambda$_.\n",
    "\n",
    "  - Si $\\mathcal{D}(\\lambda)$ es discreta, tiene una función de masa $\\operatorname{p}_{\\lambda}(x)$.\n",
    "  - Si $\\mathcal{D}(\\lambda)$ es continua, tiene una función de densidad $f_{\\lambda}(x)$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tenemos una serie de $n$ observaciones _independientes_ $\\{x_j\\}_{j=1}^n$ de $X$. Definimos la **verosimilitud** (**likelihood**) de los datos:\n",
    "  - Si $\\mathcal{D}(\\lambda)$ es _discreta_, la verosimilitud de $\\lambda$ es la probabilidad de los datos si el parámetro es $\\lambda$:\n",
    "$$\n",
    "\\mathcal{L}\\left(\\lambda|\\:\\{x_j\\}_{j=1}^n\\right) = P\\left(\\:\\{x_j\\}_{j=1}^n|\\lambda\\right) = \\prod_{j=1}^n \\operatorname{p}_{\\lambda}(x_j)\n",
    "$$\n",
    "  - Si $\\mathcal{D}(\\lambda)$ es _continua_, la verosimilitud de $\\lambda$ es la función de densidad evaluada en los datos, si el parámetro es $\\lambda$:\n",
    "$$\n",
    "\\mathcal{L}\\left(\\lambda|\\:\\{x_j\\}_{j=1}^n\\right) =  f(\\:\\{x_j\\}_{j=1}^n|\\lambda)=\\prod_{j=1}^n f_{\\lambda}(x_j)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La *estimación de máxima verosimilitud del parámetro $\\lambda$* es el valor de $\\lambda$ que _maximiza la verosimilitud_:\n",
    "$$\n",
    "\\lambda^* = \\operatorname{argmax}_{\\lambda} \\mathcal{L}\\left(\\lambda|\\:\\{x_j\\}\\right)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Observaciones importantes\n",
    "\n",
    "Es importante recalcar que\n",
    " - Es necesario que las observaciones sean independientes para poder escribir $P(\\:(x_j)|\\lambda)=\\prod_{j=1}^n p_{\\lambda}(x_j)$ y $f(\\:(x_j)|\\lambda)=\\prod_{j=1}^n f_{\\lambda}(x_j)$.\n",
    " - La verosimilitud podría no tener un máximo en el conjunto de parámetros válidos, o podría tener más de un máximo. Sin embargo, en los casos habituales, el máximo existe y es único.\n",
    " - La verosimilitud de $\\lambda$ no es la probabilidad de $\\lambda$ condicionada a los datos. Observa la diferencia cuando calculamos la probabilidad $P\\left(\\lambda|\\:\\{x_j\\}_{j=1}^n\\right)$ con el teorema de Bayes (asumiendo que $\\lambda$ solo puede tomar una cantidad finita de valores):\n",
    "$$\n",
    "P\\left(\\lambda|\\:\\{x_j\\}_{j=1}^n\\right) = \\frac{ P\\left(\\:\\{x_j\\}_{j=1}^n|\\lambda\\right)P(\\lambda)}{P\\left(\\:\\{x_j\\}_{j=1}^n\\right)}\n",
    "$$\n",
    " - Por ello nunca calcularemos la probabilidad de que el valor del parámetro sea menor que un cierto umbral, ni la media o dispersión de $\\lambda$, sino únicamente el valor $\\lambda^*$ que maximiza $\\mathcal{L}\\left(\\lambda|\\:(x_j)\\right)$. Al menos, no dentro del contexto de _máxima verosimilitud_."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Algunos ejemplos analíticos\n",
    "\n",
    "### Bernoulli\n",
    "\n",
    "Por ejemplo, desconocemos la proporción $p$ de personas vegetarianas en un congreso, pero en el primer turno de comida, han llegado 20 personas, de las cuales 4 eran vegetarianas. Podemos asumir que esas 20 personas estaban elegidas al azar porque no venían del mismo grupo. ¿Cuál es la estimación de máxima verosimilitud para $p$?\n",
    "\n",
    "Hemos observado un fenómeno aleatorio de tipo Bernouilli, con $n=20$ observaciones, pero no conocemos $p$. La verosimilitud (*likelihood*) es\n",
    "\n",
    "$$\n",
    "\\mathcal{L}\\left(p |\\:(x_j)\\right) = \n",
    "P\\left(\\:\\{x_j\\}_{j=1}^n| p \\right) =\n",
    "\\prod_{j=1}^n \\operatorname{p}_{Ber(p)}(x_j) = \n",
    "\\prod_{j=1}^n p^{x_j}(1-p)^{1-x_j} =\n",
    "p^{\\sum_{j=1}^n x_j}(1-p)^{n-\\sum_{j=1}^n x_j} =\n",
    "p^{e}(1-p)^{f}\n",
    "$$\n",
    "donde $e$ es el número de éxitos y $f$ el número de fracasos.\n",
    "Para encontrar el máximo de $\\mathcal{L}(p)$ derivamos respecto a $p$:\n",
    "$$\n",
    "\\frac{d \\mathcal{L}}{d p} = e p^{e-1}(1-p)^f - f p^e(1-p)^{f-1} = (e(1-p) - f p)p^{e-1}(1-p)^{f-1}\n",
    "$$\n",
    "Comprobamos que\n",
    "$$\n",
    "0 = \\frac{d \\mathcal{L}}{d p} = (e(1-p) - f p)p^{e-1}(1-p)^{f-1} \\Leftrightarrow e = (e+f)p\\Leftrightarrow p = \\frac{e}{e+f} = \\frac{e}{n} \n",
    "$$\n",
    "Es *intuitivo*, ¿no?: el estimador de máxima verosimilitud es la proporción de vegetarianos $p^*=4/20=0.20$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observamos que la verosimilitud sólo depende del número de éxitos $e$ y del número de fracasos $f$. Es indiferente si los éxitos vinieron primero y los fracasos después, o viceversa.\n",
    "\n",
    "Decimos que $e$ y $f$ son **estadísticos suficientes (sufficient statistics)** para la distribución de Bernoulli."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "jp-MarkdownHeadingCollapsed": true,
    "tags": []
   },
   "source": [
    "### Binomial\n",
    "\n",
    "En el ejemplo anterior, podemos pensar que en vez de 20 observaciones de un fenómeno aleatorio de tipo Bernouilli, hemos hecho una única observación de una Binomial $Y$ con $n=20$ observaciones, pero no conocemos $p$. El dato observado es $y=4$.\n",
    "\n",
    "La verosimilitud (*likelihood*) para $Y$.\n",
    "\n",
    "$$\n",
    "\\mathcal{L}\\left(p |\\:y\\right) = \n",
    "P\\left(y | p \\right) =\n",
    "\\operatorname{p}_{Bin(n,p)}(y) = \n",
    "{n \\choose y} p^{y}(1-p)^{n-y} = \n",
    "{20 \\choose 4} p^{4}(1-p)^{16}\n",
    "$$\n",
    "En resumen, la verosimilitud es igual a la anterior, pero multiplicada por el factor ${20 \\choose 4}$.\n",
    "Por lo tanto, el valor $p^*$ que maximiza la verosimilitud es el mismo: $p^*=4/20=0.20$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Ejemplo\n",
    "\n",
    "Se quiere estimar la población de un animal salvaje.\n",
    "\n",
    "Se capturan $10$ animales y son marcados. Al cabo del tiempo se capturan $20$ animales, y se observa que hay $4$ marcados. Utiliza el estimador de máxima verosimilitud para la población $n$.\n",
    "\n",
    "En este caso la proporción de animales marcados es $\\frac {10}{n}$, y tenemos una binomial $B(20,\\frac{10}{n})$ y debemos maximizar la probabilidad de obtener $4$, es decir debemos encontrar $n$ tal que \n",
    "$$\n",
    "0=\\frac{d\\mathcal{L}(n|y)}{dn}=\\frac{d\\left\\{\\log\\left(\\binom{20}{4}\\left(\\frac {10}n\\right)^4\\left(1-\\frac {10}n\\right)^{16}\\right)\\right\\}}{dn}=-\\frac{20}{n}+\\frac{16}{n-10}\n",
    "$$\n",
    "\n",
    "Con lo que $n=50$, lo que tiene sentido pues la proporción $\\frac{10}{50}$ coincide con la proporción $\\frac4{20}$ de animales marcados de entre los capturados."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exponencial\n",
    "\n",
    "Las llamadas telefónicas a cierto servicio técnico siguen un proceso de Poisson, pero desconocemos la tasa $\\lambda$ del proceso. Sin embargo, tenemos datos $\\{t_j\\}_{j=1}^n$ de _tiempos entre llamadas_.\n",
    "La verosimilitud usa ahora la función de densidad\n",
    "\n",
    "$$\n",
    "\\mathcal{L}\\left(\\lambda|\\:\\{t_j\\}_{j=1}^n\\right) =  \n",
    "\\prod_{j=1}^n f_{\\lambda}(t_j) =  \n",
    "\\prod_{j=1}^n \\lambda e^{-\\lambda t_j} =\n",
    "\\lambda^n e^{-\\lambda \\sum_{j=1}^n t_j} =\n",
    "\\lambda^n e^{-\\lambda T}\n",
    "$$\n",
    "Observamos que la verosimilitud sólo depende del tiempo total observado $T=\\sum_{j=1}^n t_j$, y del número de llamadas $n$.\n",
    "\n",
    "Decimos que $T$ y $n$ son **estadísticos suficientes (sufficient statistics)** para la distribución exponencial.\n",
    "\n",
    "El máximo de la verosimilitud es fácil de encontrar: $\\lambda^* = \\frac{n}{T}$, y de nuevo es intuitivo: como la media de un proceso de Poisson es $\\lambda$, fijamos $\\lambda^*$ igual al número de observaciones dividido por la longitud total del intervalo observado."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Normal\n",
    "\n",
    " - $X\\sim \\mathcal{N}(\\mu, \\sigma)$, pero no conocemos $\\mu$ ni $\\sigma$\n",
    " - $\\{x_j\\}_{j=1}^n$ son observaciones independientes de $X$\n",
    "Entonces la estimación de máxima verosimilitud de $\\mu$ y $\\sigma$ es:\n",
    "$$\n",
    "{\\displaystyle {\\mu^* }={\\bar {x}}=\\sum _{i=1}^{n}{\\frac {\\,x_{i}\\,}{n}}}\n",
    "$$\n",
    "\n",
    "y\n",
    "\n",
    "$$\n",
    "{\\displaystyle {\\sigma^* }^{2}={\\frac {1}{n}}\\sum _{i=1}^{n}(x_{i}-\\bar{x} )^{2}.}\n",
    "$$\n",
    "\n",
    "Es decir: $\\mu^*$ es la media muestral, y ${\\sigma^*}^2$ es la varianza muestral.\n",
    "\n",
    "En la wikipedia podéis leer el [cálculo del estimador](https://en.wikipedia.org/wiki/Maximum_likelihood_estimation#Continuous_distribution,_continuous_parameter_space)\n",
    "\n",
    "- *Atención*: a menudo la varianza muestral no se define como $\\frac {1}{n}\\sum _{i=1}^{n}(x_{i}-\\bar{x} )^{2}$, sino como ${\\frac {1}{n-1}}\\sum _{i=1}^{n}(x_{i}-\\bar{x} )^{2}$, por otros motivos que no estudiaremos.\n",
    "Es importante saber cual de las dos calcula cada librería de software, aunque en la práctica la diferencia es pequeña."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Uniforme\n",
    "\n",
    "En este caso la función de densidad es constante $f=\\frac1{b-a}$, y la incógnita es el intervalo en el que está definida. Tenemos  $L=-n\\log(b-a)$ y para maximizarla, se toman como límites del intervalo el mayor y menor valor de los datos."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Numéricamente\n",
    "\n",
    "\n",
    "Para las distribuciones anteriores no es necesario hacer el cálculo de forma numérica porque lo podemos hacer de forma exacta, pero para otras distribuciones no es posible hacer el cálculo exacto.\n",
    "\n",
    "Por ejemplo, la distribución de  **Weibull** , habitual al estudiar tiempos de vida útil de componentes, o datos de oleaje, no admite una expresión cerrada para la estimación de máxima verosimilitud. Concretamente su función de densidad viene dada por \n",
    "$$\n",
    "f(x)=\\lambda\\alpha(\\lambda x)^{\\alpha-1}e^{-(\\lambda x)^\\alpha} \\text{ si } x>0\n",
    "$$\n",
    "y al hacer el cálculo para maximizar la verosimilitud obtenemos\n",
    "$$\n",
    "\\frac1\\lambda=\\left(\\frac 1n\\sum_{i=1}^nx_i^\\alpha\\right)^{1/\\alpha},\n",
    "$$\n",
    "mientras que $\\alpha$ es la solución de la ecuación \n",
    "$$\n",
    "0=\\sum_{i=1}^n\\log(x_i)+\\frac1\\alpha-\\frac{\\sum_{i=1}^n\\log(x_i)x_i^\\alpha}{\\sum_{i=1}^nx_i^\\alpha},\n",
    "$$\n",
    "que no se puede calcular explícitamente.\n",
    "\n",
    "En estos casos la estimación de máxima verosimilitud también se puede hallar numéricamente, pidiendo al ordenador que maximize la función de verosimilitud.\n",
    "\n",
    "La librería ``scipy.stats`` incluye los métodos ``st.weibull_min.fit``, ``st.expon.fit``, ``st.gamma.fit``, ``st.norm.fit``, etc (*sólo para distribuciones continuas*):\n",
    "\n",
    "```python\n",
    "datos = np.array([2.3, 2.4, 2.1, 2.7,1.8,2.3,2.6])\n",
    "loc0, scale0 = st.norm.fit(datos)\n",
    "```\n",
    "\n",
    "Donde ``loc`` y ``scale`` son los dos parámetros que ``scipy.stats`` reconoce para ``scipy.stats.norm``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2.3142857142857145 0.2799416848895061\n",
      "(2.3142857142857145, 0.2799416848895061)\n"
     ]
    }
   ],
   "source": [
    "datos = np.array([2.3, 2.4, 2.1, 2.7,1.8,2.3,2.6])\n",
    "print(datos.mean(), datos.std())\n",
    "print(st.norm.fit(datos))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "Hay que tener cuidado: a veces ``scipy.stats`` tiene parámetros adicionales que no queremos ajustar, porque conocemos el valor, o por lo que sea. Por ejemplo, ``st.expon``, además del parámetro ``scale`` tiene uno ``loc`` que normalmente queremos dejar a 0.\n",
    "\n",
    "Si queremos fijar un parámetro, podemos pasarle más argumentos a ``st.expon.fit``:\n",
    "\n",
    "```python\n",
    "tiempos = np.array([1.1, 3, 2.8, 2.3, 2])\n",
    "loc0, scale0 = st.expon.fit(tiempos, floc=0)\n",
    "```\n",
    "\n",
    "Donde:\n",
    " - ``loc`` y ``scale`` son los dos parámetros que ``scipy.stats`` reconoce para ``scipy.stats.expon``\n",
    " - ``floc=0`` indica que queremos fijar ``loc`` a ``0``, porque no queremos que la desplace.\n",
    " - ``tiempos`` son los datos\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.44642857142857145\n",
      "0.44642857142857145\n"
     ]
    }
   ],
   "source": [
    "tiempos = np.array([1.1, 3, 2.8, 2.3, 2])\n",
    "print(len(tiempos)/sum(tiempos))\n",
    "loc0, scale0 = st.expon.fit(tiempos, floc=0)\n",
    "print(1/scale0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.1 0.8771929824561406\n"
     ]
    }
   ],
   "source": [
    "tiempos = np.array([1.1, 3, 2.8, 2.3, 2])\n",
    "loc0, scale0 = st.expon.fit(tiempos)\n",
    "print(loc0, 1/scale0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(2.185016906733729, 0, 0.9266032186937583)"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "datos = np.array([1.028, 1.58 , 0.45, 0.84 , 0.558, 0.436])\n",
    "st.weibull_min.fit(datos, floc=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "samples = st.weibull_min.rvs(2.185016906733729,  loc=0, scale=0.9266032186937583, size = 10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([1.11120714, 1.35764815, 0.93233313, 1.67107083, 1.30611209,\n",
       "       0.5372187 , 0.48914311, 0.53029739, 1.17365846, 0.82500659])"
      ]
     },
     "execution_count": 37,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "samples"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Consistencia\n",
    "\n",
    "Una justificación del método de máxima verosimilitud es la siguiente: si generas una muestra de tamaño $n\\rightarrow\\infty$ de una distribución paramétrica $\\mathcal{D}(\\theta)$ para un valor concreto del parámetro $\\theta_0$, la estimación de máxima verosimilitud de $\\theta$ asociada a los datos $\\{x_j\\}_{j=1}^n$ converge a $\\theta_0$.\n",
    "Esta propiedad se denomina la **consistencia (consistency)** del estimador. Esta propiedad no se cumple para todas las familias paramétricas, pero sí se cumple para las familias paramétricas habituales."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(19.999708972745495, 3.986021320521417)"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "#Comprobamos la consistencia\n",
    "datos = st.norm(loc=20, scale=4).rvs(10000)\n",
    "st.norm.fit(datos)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Estimación de máxima verosimilitud (0.049675780451967674, 3.9312465498590488)\n",
      "Estimación dejando fijo loc=0 (0, 3.9622843388317457)\n"
     ]
    }
   ],
   "source": [
    "#Comprobamos la consistencia\n",
    "datos = st.rayleigh(scale=4).rvs(1000)\n",
    "print('Estimación de máxima verosimilitud', st.rayleigh.fit(datos))\n",
    "print('Estimación dejando fijo loc=0',st.rayleigh.fit(datos, floc=0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Estimación de máxima verosimilitud (1.96638100234754, -0.008998809926339526, 1.4289777995141515)\n",
      "Estimación dejando fijo loc=0 (1.9470369355075712, 0, 1.418039020471531)\n"
     ]
    }
   ],
   "source": [
    "#Comprobamos la consistencia\n",
    "datos = st.weibull_min(c=2, scale=1.4).rvs(1000)\n",
    "print('Estimación de máxima verosimilitud', st.weibull_min.fit(datos))\n",
    "print('Estimación dejando fijo loc=0',st.weibull_min.fit(datos, floc=0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sin embargo, este método también tiene inconvenientes, que veremos la semana que viene..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Máxima verosimilitud para la Normal multivariable\n",
    "\n",
    " - $X=(X_1,\\dots,X_d)$ es un vector aleatorio que sigue una distribución Normal multivariable: $X\\sim \\mathcal{N}(\\overrightarrow{\\mu}, \\Sigma)$, pero no conocemos $\\overrightarrow{\\mu}$ ni $\\Sigma$.\n",
    " - $\\{\\overrightarrow{x_i}\\}_{i=1}^n$ son observaciones independientes de $X$.\n",
    "\n",
    "Entonces \n",
    " - la estimación de máxima verosimilitud de $\\overrightarrow{\\mu}$ es $\\overrightarrow{\\mu}^*$, el **vector de medias muestrales**: \n",
    "$$\n",
    "\\overrightarrow{\\mu}^*=\\bar{x} =\\frac {1}{n}\\sum _{i=1}^{n}{\\overrightarrow{x_i}}\n",
    "$$\n",
    " Es decir, $\\bar{x}$ es el vector de $\\mathbb{R}^d$ cuya componente $j$-ésima es el promedio de $X_j$ en la muestra:\n",
    "$$\\bar {x}_j=\\frac {1}{n}\\sum _{i=1}^{n}{(x_j)_{i}}.$$\n",
    " - la estimación de máxima verosimilitud de $\\Sigma$ es la **matriz de covarianzas muestral**: \n",
    " $$\\Sigma^*={\\frac {1}{n}}\\sum _{i=1}^{n}(\\overrightarrow{x_i}-\\bar{x})(\\overrightarrow{x_i}-\\bar{x})^t.$$\n",
    " Es decir, $\\Sigma^*$ es la matriz $d\\times d$ cuya componente $(j,k)$ es la covarianza entre $X_j$ y $X_k$ en la muestra:\n",
    " $$cov(X_j,X_k)={\\frac {1}{n}}\\sum _{i=1}^{n}((x_j)_{i}-\\bar{x}_j)((x_k)_{i}-\\bar{x}_k).$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
