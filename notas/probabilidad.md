% Introducción a la Probabilidad
% Pablo Angulo
% ETSIN

# Probabilidad

## Objetivo

::: incremental

 - _Estudiar procesos, experimentos, sucesos, cuyo resultado no se puede predecir (tanto si es por desconocimiento o porque es intrínsecamente aleatorio)._
 - _Hablar de la certeza o incertidumbre con propiedad._
 - _**Cuantificar** la incertidumbre._

:::

# Experimentos

## Definiciones

::: incremental

 - **Experimento aleatorio (random experiment)**. Cualquier fenómeno cuyo resultado no se puede predecir (tanto si es por desconocimiento o porque es intrínsecamente aleatorio).
 - **Resultado (outcome)**. Cada observación del experimento termina en un resultado, que es aquella cantidad, u objeto que registramos y que queremos estudiar.
 - **Espacio muestral (Sample space).** El conjunto de todos los posibles resultados del experimento.
 - **Evento o suceso (event).** Cualquier subconjunto del espacio muestral.

:::

## Algunos tipos de evento especiales

  **Evento o suceso (event).** Cualquier subconjunto del espacio muestral.

::: incremental

 - **Evento elemental (simple event).** Un evento con un sólo elemento.
 - **Evento compuesto (compound event).** Un evento con más de un elemento.
 - **Evento vacío ($\emptyset$).** El único evento que no tiene ningún elemento.
 - **Evento total ($\Omega$ o $E$).** El único evento con todos los posibles resultados. Es igual al espacio muestral.

:::

## Ejemplo: una moneda

::: incremental

 - **Experimento aleatorio (random experiment)**. Lanzar una moneda y anotar el resultado.
 - **Resultado (outcome)**. Puede ser _cara_ (__H: head__) o _cruz_ (__T: tails__).
 - **Espacio muestral (Sample space).** El conjunto con dos elementos $\Omega=\{H, T\}$.
 - **Evento o suceso (event).** Cuatro posibilidades: $\emptyset$, $\{H\}$, $\{T\}$, $\{H,T\}=\Omega$
 - **Evento elemental (simple event).** Los eventos $\{H\}$ y $\{T\}$.
 - **Evento compuesto (compound event).** El único evento compuesto en este caso es $\Omega$

:::

## Ejemplo: un dado

::: incremental

 - **Experimento aleatorio (random experiment)**. Lanzar un dado y anotar el resultado.
 - **Resultado (outcome)**. Puede ser _1_, _2_, _3_, _4_, _5_ y _6_.
 - **Espacio muestral (Sample space).** El conjunto con seis elementos $\Omega=\{1,2,3,4,5,6\}$.
 - **Evento o suceso (event).** $2^6$ posibilidades, porque cada resultado puede estar o no estar en un evento. Ejemplos
    - los números pares: $\{2,4,6\}$
    - los números mayores o iguales que 4: $\{4,5,6\}$.
 - **Evento elemental (simple event).** Los eventos $\{1\}$, $\{2\}$, ..., $\{6\}$.

:::


## Ejemplo: dos monedas indistinguibles

::: incremental

 - **Experimento aleatorio (random experiment)**. Lanzar dos monedas indistinguibles y anotar el número de caras y cruces.
 - **Resultado (outcome)**. Puede ser _2 caras_ (__2H__),  _1 cara y 1 cruz_ (__HT__) o _dos cruces_ (__2T__).
 - **Espacio muestral (Sample space).** El conjunto con tres elementos $\Omega=\{2H, HT, 2T\}$.
 - **Evento o suceso (event).** $2^3=8$ posibilidades: $\emptyset$, $\{2H\}$, $\{HT\}$, $\{2T\}$, $\{2H,HT\}$, $\{2H,2T\}$, $\{HT,2T\}$, $\{2H,HT,2T\}=\Omega$
 - **Evento elemental (simple event).** Los eventos $\{2H\}$, $\{HT\}$ y $\{2T\}$.
 - **Evento compuesto (compound event).** $\{2H,HT\}$, $\{2H,2T\}$, $\{HT,2T\}$, $\{2H,HT,2T\}=\Omega$

:::


## Ejemplo: dos monedas distintas

::: incremental

 - **Experimento aleatorio (random experiment)**. Lanzar dos monedas distintas y anotar si cada una es __H__ o __T__.
 - **Resultado (outcome)**. Puede ser _las dos caras_ (__HH__),  _la primera cara y la segunda cruz_ (__HT__), viceversa (__TH__) o _las dos cruces_ (__TT__).
 - **Espacio muestral (Sample space).** El conjunto con cuatro elementos $\Omega=\{HH, HT, TH, TT\}$.
 - **Evento o suceso (event).** $2^4=16$ posibilidades
 - **Evento elemental (simple event).** Los eventos $\{HH\}$, $\{HT\}$, $\{TH\}$ y $\{TT\}$.

:::


## Ejemplo: el peso de un pez

::: incremental

 - **Experimento aleatorio (random experiment)**. Pescar un pez y anotar su peso.
 - **Resultado (outcome)**. Un número real positivo.
 - **Espacio muestral (Sample space).** El conjunto de los números reales positivos.
 - **Evento o suceso (event).** Cualquier subconjunto de $\mathbb{R}^+$: infinitas posibilidades.
     + pesa más de 3 kg: $[3,\infty)$
     + pesa entre 3 y 5 kg: $[3,5)$

:::


## Ejemplo: el peso y la altura de un pez

::: incremental

 - **Experimento aleatorio (random experiment)**. Pescar un pez y anotar su peso y su altura.
 - **Resultado (outcome)**. Dos números reales positivos.
 - **Espacio muestral (Sample space).** El conjunto de los pares de números reales positivos $\mathbb{R}^+\times \mathbb{R}^+$.
 - **Evento o suceso (event).** Cualquier subconjunto de $\mathbb{R}^+\times \mathbb{R}^+$: infinitas posibilidades.
     + pesa más de 3 kg, mide menos de 1m: $[3,\infty)\times[0,1]$

:::

# Medidas de probabilidad

## Idea

Una _medida de probabilidad_ es una forma "razonable" de medir la incertidumbre:

  - es más probable que al lanzar un dado salga un par que un múltiplo de 3
  - es imposible que al lanzar un dado el resultado sea a la vez par e impar
  - la probabilidad de que una persona mida más de dos metros es menor que la probabilidad de que una persona mida más de 1.60m

¿Qué significa medir la incertidumbre de un experimento?

. . .

  _Asignar a cada evento un número real entre $0$ y $1$, donde $0$ significa **imposible** y $1$ significa **seguro**_.

## Axiomas de la probabilidad

Una medida de probabilidad es una función que a cada subconjunto del espacio muestral le asigna un número real, de modo que además:

 - **Las probabilidades son positivas**:  $P({A}) \geq 0$.
 - **La probabilidad del espacio total es $1$**:  $P({E}) = 1$.
 - **Probabilidad de una unión de conjuntos disjuntos**: $P(A\cup B) = P(A) + P(B)$, \textit{siempre que} $A\cap B=\emptyset$.

. . .

Es decir, cualquier función que cumpla estos _axiomas_ es una medida de probabilidad.

_Pero si cumple estos axiomas, entonces tiene muchas más propiedades_

## Consecuencias de los axiomas

 - **Probabilidad del suceso vacío**  $P(\emptyset) = 0$.
 - **Probabilidad del complemento**  $P(A^C) =1 - P(A)$.
 - **Probabilidad de una union de conjuntos no disjuntos** $P(A \cup B) = P(A) + P(B) - P(A \cap B)$

. . .

... y muchas otras propiedades.

## Ejemplo primigenio de medida de probabilidad

**Cardano's Definition of Probability**

_If the number of outcomes is finite and all outcomes are equally likely_, the probability of an event $A$ happening is:

$$P_{\textrm{Cardano}}(A) = \frac{\textnormal{number of outcomes favorable to $A$}}{\textnormal{number of outcomes}}$$

## Ejemplo: una moneda

::: incremental

 - **Experimento aleatorio (random experiment)**. Lanzar una moneda y anotar el resultado.
 - **Prob de un suceso elemental**. $P(H)=P(T)=1/2$.
 - **Prob de vacío y total**. $P(\emptyset)=0$ $P(\Omega)=1$.

:::

## Ejemplo: un dado

::: incremental

 - **Experimento aleatorio (random experiment)**. Lanzar un dado y anotar el resultado.
    - Prob de un suceso elemental_ $P(1)=P(2)=\dots=P(6)=1/6$.
    - P(el resultado de lanzar el dado es par): $P(\{2,4,6\}) = P(2) + P(4) + P(6) = 1/2$.
    - P(resultado mayor o igual que 4): $P(\{4,5,6\}) = P(4) + P(5) + P(6) = 1/2$.
    - P(par y además impar): $P(\{2,4,6\}\cap \{1,3,5\}) = P(\emptyset) =0$.
 - **Usando la regla de Cardano**:
    - P(el resultado de lanzar el dado es par): $P(par) = \frac{\textnormal{3 casos favorables al evento "par"}}{\textnormal{6 resultados posibles}}$.

:::


## Ejemplo: dos monedas indistinguibles

::: incremental

 - **Experimento aleatorio (random experiment)**. Lanzar dos monedas indistinguibles y anotar el número de caras y cruces.
 - **Resultado (outcome)**. Puede ser _2 caras_ (__2H__),  _1 cara y 1 cruz_ (__HT__) o _dos cruces_ (__2T__).
   - Es más probable obtener HT que 2H => **no se puede aplicar la regla de Cardano**.

:::


## Ejemplo: dos monedas distintas

::: incremental

 - **Experimento aleatorio (random experiment)**. Lanzar dos monedas distintas y anotar si cada una es __H__ o __T__.
 - **Resultado (outcome)**. Puede ser _las dos caras_ (__HH__),  _la primera cara y la segunda cruz_ (__HT__), viceversa (__TH__) o _las dos cruces_ (__TT__).
    - Todos los resultados son equiprobables: $P(HH)=P(HT)=P(TH)=P(TT)=1/4$.
    - P(al menos una cruz): $P(\{HT, TH, TT\}) = P(\{HT\}) + P(\{TH\}) + P(\{TT\}) = 3/4.$
 - **Usando la regla de Cardano**:
    - P(al menos una cruz): $P(\{HT, TH, TT\}) = \frac{\textnormal{3 casos favorables}}{\textnormal{4 casos posibles}} = 3/4$.

:::


## Ejemplo: el peso de un pez

::: incremental

 - **Experimento aleatorio (random experiment)**. Pescar un pez y anotar su peso.
 - **Resultado (outcome)**. Un número real positivo
    - Infinitas posibilidades => **no se puede aplicar la regla de Cardano**.

:::
