P = st.poisson(mu=1.8)
N = 1000
#A sample that takes values in the integers
sample = P.rvs(N)
max_integer = 10
plt.hist(sample, bins=[k+0.5 for k in range(-1,max_integer+1)], density=1,alpha=0.8)
