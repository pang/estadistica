E = st.expon(scale=3)
xmin, xmax = -3, 3 #plotting interval
N = 100 # number of subdivions
xs = np.linspace(xmin, xmax, N)
ys = E.pdf(xs) #use E.cdf(xs) for cumulative distribution function
plt.plot(xs, ys, 'g')
